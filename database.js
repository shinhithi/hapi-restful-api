/**
 * There's not much value to this file
 * It's just a database stub to simulate calls
 *   to a db storage engine.
 * Pay little attention to this file in the context
 *   of this example.
 **/

'use strict';

var mysql = require('mysql');
var Promise = require('bluebird');
var using = Promise.using;

Promise.promisifyAll(require('mysql/lib/Connection').prototype);
Promise.promisifyAll(require('mysql/lib/Pool').prototype);


var pool = mysql.createPool({
  host     : 'localhost',
  user     : 'jera_api',
  password : '123456',
  database : 'jera_api'
});

function getConnection() {
     return pool.getConnectionAsync().disposer(function (connection) {
        return connection.destroy();
     });
}

function getQuery(command) {
    return using(getConnection(), function (connection) {
        return connection.queryAsync(command);
    });
}

module.exports = function() {
    var store = {};

    function Database() {};

    Database.prototype.getAll = function () {
        return getQuery('SELECT * FROM devices;').then(function (r) {
          return r;
        });     
    };

    Database.prototype.get = function(key) {
        var value;
        return value = typeof store !== 'undefined' && store !== null ? store[key] : void 0;
    };

    Database.prototype.set = function(key, value) {
        store[key] = value;
        return store[key];
    };

    // Used in tests
    Database.prototype.clear = function() {
        store = {};
    };

    return new Database();
};
